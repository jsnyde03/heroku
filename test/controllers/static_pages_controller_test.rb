require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Garrett County, MD"
  end

  test "should get history" do
    get :history
    assert_response :success
    assert_select "title", "History | Garrett County, MD"
  end

  test "should get people" do
    get :people
    assert_response :success
    assert_select "title", "Famous Visitors | Garrett County, MD"
  end

  test "should get attractions" do
    get :attractions
    assert_response :success
    assert_select "title", "Attractions | Garrett County, MD"
  end

  test "should get posts" do
    get :posts
    assert_response :success
    assert_select "title", "Posts | Garrett County, MD"
  end
  
  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | Garrett County, MD"
  end
  
  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact | Garrett County, MD"
  end
end
