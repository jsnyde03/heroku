class StaticPagesController < ApplicationController
  def home
  end

  def history
  end

  def people
  end

  def attractions
  end

  def posts
    if logged_in?
      @micropost = current_user.microposts.build 
      @feed_items = current_user.micropost_feed.paginate(page: params[:page])
      @messageFeed_items = current_user.message_feed.paginate(page: params[:page])
  end
  
  def count
    count.py
  end
end
end
